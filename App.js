import axios from 'axios';
import React, {useState, useEffect, useMemo} from 'react';
import {SafeAreaView, Button, Text, ActivityIndicator} from 'react-native';
import Header from './components/Header';
import List from './components/List';
import SearchBar from './components/SearchBar';

const App = () => {
  const [items, setItems] = useState(null);
  const [input, setInput] = useState('');

  const fetchData = () => {
    // fetch('https://breakingbadapi.com/api/characters')
    //   .then((response) => response.json())
    //   .then((json) => setItems(json));
    axios
      .get('https://breakingbadapi.com/api/characters')
      .then((response) => setItems(response.data))
      .catch((err) => console.log(err));
  };

  useEffect(() => fetchData(), []);

  const searchUser = (input) => {
    if (!items) {
      return null;
    }
    console.log(input);
    return items.filter((user) => user.name.toLowerCase().includes(input));
  };

  const filteredUsers = useMemo(() => searchUser(input), [items, input]);

  return (
    <SafeAreaView>
      <Header title={'LISTA'} />
      {filteredUsers ? (
        <List items={filteredUsers} />
      ) : (
        <ActivityIndicator size={'large'} color={'brown'} />
      )}
    </SafeAreaView>
  );
};




export default App;
