import React from 'react';
import {View, Text, StyleSheet, TouchableOpacity} from 'react-native';

const Header = () => {
  return (
    <View style={styles.header}>
      <View>
        <Text style={styles.title}>PERSONAJES</Text>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  header: {
    width: '100%',
    height: 50,
    flexDirection: 'row',
    backgroundColor: '#7A3714',
    justifyContent: 'flex-start',
    alignItems: 'center',
    paddingLeft: 20,
  },
  title: {
    color: '#fff',
    fontSize: 20,
    fontWeight: 'bold',
  },
  appButton: {
    backgroundColor: 'blue',
    height: 60,
    width: 160,
    justifyContent: 'center',
    alignItems: 'center',
  },
  appTitle: {
    color: '#fff',
  },
});

export default Header;
