import React from 'react';
import {FlatList, ScrollView} from 'react-native';
import Item from './Item';

function List({items}) {
  return (
    <FlatList
      renderItem={Item}
      data={items}
      keyExtractor={(item) => String(item.char_id)}
      initialNumToRender={10}
    />
  );
}

export default List;
