import React from 'react';
import {View, Text, Image, StyleSheet, Button} from 'react-native';
import axios from 'axios';

const Item = ({item}) => {
  const {name, birthday, nickname, status, img, char_id} = item;

  const deleteUser = (char_id) => {
    axios
      .delete(`https://breakingbadapi.com/api/characters${char_id}`)
      .then((response) => console.log(response.status))
      .catch((err) => console.log(err));
  };
  return (
    <View style={styles.item}>
      <View style={styles.container}>
        <View>
          <Image style={styles.image} source={{uri: img}} />
        </View>
        <View style={styles.titleWrapper}>
          <View style={{flexDirection: 'row'}}>
            <Text style={styles.title}>Nombre:</Text>
            <Text style={styles.title}>{name}</Text></View>
          <Text style={styles.subtitle}>{birthday}</Text>
          <Text style={styles.subtitle}>{nickname}</Text>
          <Text style={styles.subtitle}>{status}</Text>
          <View style={styles.buttonWrapper}>
            <Button title={'Ver mas'} onPress={() => console.log(char_id)} />
      </View>
        </View>
      </View>
      {/* <View style={styles.container}>
        <Text style={styles.description} numberOfLines={2} ellipsizeMode="tail">
          {website}
        </Text>
      </View> 
      <View style={styles.buttonWrapper}>
        <Button title={'Ver mas'} onPress={() => console.log(char_id)} />
      </View>*/}
    </View>
  );
};

const styles = StyleSheet.create({
  image: {
    height: 150,
    width: 100,
  },
  item: {
    width: '100%',
    padding: 10,
    borderBottomWidth: 1,
    borderBottomColor: '#ccc',
  },
  container: {
    flexGrow: 0.5,
    flexDirection: 'row',
  },
  title: {
    fontSize: 18,
    fontWeight: 'bold',
    color: 'black',
  },
  subtitle: {
    color: 'black',
    fontSize: 16,
  },
  titleWrapper: {
    width: '80%',
    paddingHorizontal: 10,
  },
  description: {
    marginVertical: 5,
  },
  buttonWrapper: {
    alignSelf: 'stretch',
    marginVertical: 5,
    flexDirection: 'row',
    width: '50%',
    justifyContent: 'space-between',
  },
});

export default Item;
